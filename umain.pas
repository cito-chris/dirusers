unit umain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, System.Types, System.IOUtils;

type
  TForm3 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

procedure TForm3.Button1Click(Sender: TObject);
var
  LList: TStringDynArray;
  I: Integer;
  Lfad: TWin32FileAttributeData;
  LLocalTime: TSystemTime;
  LPath: string;
  Lfilename: string;

begin
  Memo1.Lines.Clear;
  LPath := 'n:\phbook\apoteker';
  LPath := IncludeTrailingBackslash(LPath);
  if not DirectoryExists(LPath) then
    exit;

  try
    LList := TDirectory.GetFiles(LPath, '*user*.*', TSearchOption.soAllDirectories);
    for I := 0 to pred(length(LList)) do
    begin
      try
        Memo1.Lines.Add(Format('Connect /Mode=Phonebook "%s"', [LList[I]]));
        Lfilename := ExtractFileName(LList[I]);
        Lfilename := StringReplace(Lfilename, ' ', '', [rfReplaceAll]);
        Lfilename := StringReplace(Lfilename, ';', '', [rfReplaceAll]);
        Memo1.Lines.Add('Copy c:\c2\kassedknfc\20220308.txt c:\temp\kassedknfc2\' + Lfilename + '.txt /H');
        Memo1.Lines.Add('ConnectEnd');

        // if not GetFileAttributesEx(PChar(LList[I]), GetFileExInfoStandard, @Lfad) then
        // RaiseLastOSError;
        // FileTimeToSystemTime(Lfad.ftLastWriteTime, LLocalTime);
        // if SubDates(Now, SystemTimeToDateTime(LLocalTime)) > AFileAge then
        // begin
        // // C2LogAdd(LList[I] + ' Delete ' + DateTimeToStr(SystemTimeToDateTime( LLocalTime)));
        // DeleteFile(LList[I]);
        // end;

      except
      end;
    end;
  except
    on e: Exception do
    begin
    end;
  end;
end;

end.
